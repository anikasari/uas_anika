from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_user import login_required, UserManager, UserMixin
from itsdangerous import URLSafeSerializer
from sqlalchemy.ext.hybrid import hybrid_property


# Class-based application configuration
class ConfigClass(object):
    """ Flask application config """

    # Flask settings
    SECRET_KEY = '3129837126392817361923871263912873193817369128376193817369182736192873'

    # Flask-SQLAlchemy settings
    SQLALCHEMY_DATABASE_URI = 'sqlite:///app.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Flask-User settings
    USER_APP_NAME = "Flask user"
    USER_ENABLE_EMAIL = False
    USER_ENABLE_USERNAME = True
    USER_REQUIRE_RETYPE_PASSWORD = False


def create_app():
    """ Flask application factory """
    
    # Create Flask app load app.config
    app = Flask(__name__)
    app.config.from_object(__name__+'.ConfigClass')
    s = URLSafeSerializer(app.config['SECRET_KEY'])

    # Initialize Flask-SQLAlchemy
    db = SQLAlchemy(app)

    class User(db.Model, UserMixin):
        __tablename__ = 'users'
        id = db.Column(db.Integer, primary_key=True)
        active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')

        username = db.Column(db.String(100, collation='NOCASE'), nullable=False, unique=True)
        password = db.Column(db.String(255), nullable=False, server_default='')
        email_confirmed_at = db.Column(db.DateTime())

        # User information
        first_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')
        last_name = db.Column(db.String(100, collation='NOCASE'), nullable=False, server_default='')

        # mehod itsdangerous
        @hybrid_property
        def danger(self):
            return s.dumps(self.id)

    # Create all database tables
    db.create_all()

    # Setup Flask-User and specify the User data-model
    user_manager = UserManager(app, db, User)

    # The Home page is accessible to anyone
    @app.route('/')
    def home():
        return render_template('home.html')

    # The Home page is accessible to anyone
    @app.route('/gambar/')
    def gambar():
        return render_template('gambar.html')

    @app.route('/users/')
    @login_required    # User must be authenticated
    def user():
        users = User.query.all()
        return render_template('user.html',users=users)

    @app.route('/user/detail/<user_id>')
    @login_required
    def user_detail(user_id):
        user_id = s.loads(user_id)
        user = User.query.get(user_id)
        return render_template('detail.html',user=user)

    return app


# Start development web server
if __name__=='__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000, debug=True)